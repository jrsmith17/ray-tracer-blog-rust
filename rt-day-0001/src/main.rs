use std::fs::File;
use std::io::{BufRead, BufReader};

#[allow(dead_code)]
struct Vec2f {
	x: f32,
	y: f32
}

#[allow(dead_code)]
struct Vec3f {
	x: f32,
	y: f32,
	z: f32
}

struct OBJFile {
	vertex_buffer : Vec<Vec3f>,
	uv_buffer : Vec<Vec2f>,
	normal_buffer : Vec<Vec3f>
}

fn parse_obj_file(reader : BufReader<File>) -> OBJFile {
	let obj_file : OBJFile = OBJFile {
		vertex_buffer : Vec::new(),
		uv_buffer : Vec::new(),
		normal_buffer : Vec::new()
	};

	for (_index, line) in reader.lines().enumerate() {
        let line = line.unwrap(); // Ignore errors.
        let split : Vec<&str> = line.split(" ").collect::<Vec<&str>>();

        if split[0] == "v" {
        	let vertex : Vec3f = Vec3f{ x : 0 as f32, y: 0 as f32, z: 0 as f32};
        	vertex.x = split[0] as f32;
        	vertex.y = split[1] as f32;
        	vertex.z = split[2] as f32;

        	vertex_buffer.push(vertex);
        }
        else if split[0] == "vt" {
        	println!("It's a Vt!");
        }
        else if split[0] == "vn" {
        	println!("It's a Vn!");
        }
        else if split[0] == "usemtl" {
        	println!("It's a usemtl!");
        }
        else if split[0] == "s" {
        	println!("It's an s!");
        }
        else if split[0] == "f" {
        	println!("It's an f!");
        }
        else{
        	println!("something!");
        }
    }

    obj_file
}

fn main() {
	let filename = "images/obj/triangle.obj";
	let file = File::open(filename).unwrap();
    let reader : BufReader<File> = BufReader::new(file);
    parse_obj_file(reader);

    
}
